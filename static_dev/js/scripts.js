(function () {
Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {
    s = "0" + s;
  }
  return s;
};

function getSeconds (time) {
  time = time.split(':');
  var seconds = (parseInt(time[0]) * 3600) + (parseInt(time[1]) * 60) + parseInt(time[2]);
  return seconds;
}

function csrfSafeMethod(method) {
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function setTimeZoneCookie() {
  if (!Cookies.get('timezone')) {
    var timezone = (new Date()).getTimezoneOffset();
    Cookies.set('timezone', timezone, {expires: 7});
  }
}

function parseTemplates() {
  $('script[type="x-tmpl-mustache"]').each(function(index, el) {
    Mustache.parse($(el).html());
  });
}

function setCSRFHeader(xhr, settings) {
  if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
    xhr.setRequestHeader("X-CSRFToken", Cookies.get('csrftoken'));
  }
}

jQuery(document).ready(function($) {
  parseTemplates();
  setTimeZoneCookie();


  var interval;
  $('#timer').on('click', 'button', function(event) {
    event.preventDefault();
    if (/^Add$/.test(event.target.value)) {
      event.target.previousElementSibling.previousElementSibling.previousElementSibling.value = Date.now(); // start time
      $(event.target).toggleClass('green-bg red-bg').val('Stop').text('Stop');
      var time = 0;
      interval = setInterval(function() {
        time += 1;
        if (time >= 3600) {
          event.target.previousElementSibling.textContent = Math.floor(time / 3600) + ":" + Math.floor((time % 3600) / 60).pad() + ':' + (time % 60).pad() + ' h';
        } else if (time >= 60) {
          event.target.previousElementSibling.textContent = Math.floor(time / 60) + ':' + (time % 60).pad() + ' min';
        } else {
          event.target.previousElementSibling.textContent = time.pad() + ' sec';
        }
      }, 1000);
    } else {
      clearInterval(interval);
      event.target.previousElementSibling.previousElementSibling.value = Date.now(); // end time
      var startDate = event.target.previousElementSibling.previousElementSibling.previousElementSibling.value;
      var endDate = event.target.previousElementSibling.previousElementSibling.value;
      var name = event.target.parentNode.previousElementSibling.value;
      var project = event.target.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.value;
      $.ajax({
        beforeSend: setCSRFHeader,
        url: event.target.parentElement.parentElement.getAttribute('action'),
        method: 'POST',
        dataType: 'json',
        data: {name: name, project: project, start_date: startDate, end_date: endDate},
        error: function() {
          console.log("error");
        },
        success: function(response) {
          var today = response.date;
          if ($('[data-date="'+today+'"]').length) {
            var task = Mustache.render($('#task-template').html(), response.tasks[0]);
            var newDuration = $('[data-date="'+today+'"]').siblings('h5').text();
            newDuration = getSeconds(newDuration) + getSeconds(response.tasks[0].duration);
            $('[data-date="'+today+'"]').siblings('h5').text(Math.floor(newDuration / 3600) + ":" + Math.floor((newDuration % 3600) / 60).pad() + ':' + (newDuration % 60).pad());
            $('[data-date="'+today+'"]').prepend(task);
          } else {
            response.duration_sum = response.duration;
            var day = Mustache.render($('#day-template').html(), response, {'task/task.mustache': $('#task-template').html()});
            $('#days').prepend(day);
          }
        },
        complete: function() {
          event.target.previousElementSibling.textContent = '0:00 sec';
          $(event.target).toggleClass('green-bg red-bg').val('Add').text('Start');
        }
      });
    }
  });

  $('#project-form').on('submit', function(event) {
    event.preventDefault();
    var projectName = event.target.children[1].value;
    var team = event.target.children[2].value;
    $.ajax({
      beforeSend: setCSRFHeader,
      url: event.target.getAttribute('action'),
      method: 'POST',
      dataType: 'json',
      data: {name: projectName, team: team},
      error: function() {
        console.log("error");
      },
      success: function(response) {
        var project = Mustache.render($('#project-template').html(), response, {
          'team/task.mustache': $('#task-template').html(),
          'team/todo_form.mustache': $('#todo-form-template').html(),
          'team/file.mustache': $('#file-template').html(),
          'team/add_file_form.mustache': $('#file-form-template').html()
        });
        $('#projects').prepend(project);
      },
      complete: function() {
        event.target.children[1].value = '';
      }
    });
  });

  $('#projects').on('submit', '.add-todo', function(event) {
    event.preventDefault();
    var taskName = event.target.children[0].value;
    var project = event.target.children[2].value;
    $.ajax({
      beforeSend: setCSRFHeader,
      url: event.target.getAttribute('action'),
      method: 'POST',
      dataType: 'json',
      data: {task: taskName, project: project},
      error: function() {
        console.log("error");
      },
      success: function(response) {
        var task = Mustache.render($('#task-template').html(), response);
        if (event.target.parentElement.previousElementSibling.classList.contains('placeholder')) {
          event.target.parentElement.previousElementSibling.remove();
        }
        $(event.target.parentElement).before(task);
      },
      complete: function() {
        event.target.children[0].value = '';
      }
    });
  });

  $('#projects').on('change', 'input[type="file"]', function(event) {
    if(event.target.value === "") {
      event.target.parentElement.children[1].textContent = 'Select file';
    }
    else {
      var filename = event.target.value.split(/(\\|\/)/g).pop();
      event.target.parentElement.children[1].textContent = filename;
    }
  });

  $('#projects').on('submit', '.delete-file', function(event) {
    event.preventDefault();
    var pk = event.target.children[0].value;
    $.ajax({
      beforeSend: setCSRFHeader,
      url: event.target.getAttribute('action'),
      method: 'POST',
      dataType: 'html',
      data: {pk: pk},
      error: function() {
        console.log("error");
      },
      success: function(response) {
        event.target.parentElement.parentElement.remove();
      }
    });
  });

  $('#team-form').on('submit', function(event) {
    event.preventDefault();
    var name = event.target.children[1].value;
    $.ajax({
      beforeSend: setCSRFHeader,
      url: event.target.getAttribute('action'),
      method: 'POST',
      dataType: 'json',
      data: {name: name},
      error: function() {
        console.log("error");
      },
      success: function(response) {
        var team = Mustache.render($('#team-template').html(), response, {
          'team/user.mustache': $('#user-template').html(),
          'team/add_user_form.mustache': $('#user-form-template').html()
        });
        $('#teams').prepend(team);
      },
      complete: function() {
        event.target.children[1].value = '';
      }
    });
  });

  $('#teams').on('submit', '.add-member-form', function(event) {
    event.preventDefault();
    var user = event.target.children[0].value;
    var team = event.target.children[1].value;
    $.ajax({
      beforeSend: setCSRFHeader,
      url: event.target.getAttribute('action'),
      method: 'POST',
      dataType: 'json',
      data: {user: user, team: team},
      error: function() {
        console.log("error");
      },
      success: function(response) {
        var user = Mustache.render($('#user-template').html(), response);
        $(event.target.parentElement.parentElement.previousElementSibling.children[0].children[0]).after(user);
      },
      complete: function() {
        event.target.children[0].value = '';
      }
    });
  });

});
})();