from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # url(r'^', include('django.contrib.auth.urls')),
    url(r'^', include('registration.backends.simple.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('main.urls')),
    url(r'^', include('task.urls')),
    url(r'^', include('team.urls')),
]
