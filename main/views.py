from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse


def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('task.views.timer'))
    else:
        return HttpResponseRedirect(reverse('auth_login'))
