from django import template

register = template.Library()


@register.filter
def attr(obj, text):
    (attr, value) = text.split('=')
    obj.field.widget.attrs[attr] = value
    return obj
