import logging
from django import template
from django.template.base import TemplateSyntaxError, token_kwargs

register = template.Library()
logger = logging.getLogger('django.template')


class IncludeRawNode(template.Node):
    context_key = '__include_context'

    def __init__(self, template, *args, **kwargs):
        self.template = template
        super(IncludeRawNode, self).__init__(*args, **kwargs)

    def render(self, context):
        """
        Render the specified template and context. Cache the template object
        in render_context to avoid reparsing and loading when used in a for
        loop.
        """
        try:
            template = self.template.resolve(context)
            return template.template
        except Exception:
            if context.template.engine.debug:
                raise
            template_name = getattr(
                context, 'template_name', None) or 'unknown'
            logger.warning(
                "Exception raised while rendering {%% raw_include %%} for "
                "template '%s'. Empty string rendered instead.",
                template_name,
                exc_info=True,
            )
            return ''


@register.tag('raw_include')
def do_include(parser, token):
    """
    Loads a template without rendering it
    """
    bits = token.split_contents()
    if len(bits) < 2:
        raise template.TemplateSyntaxError(
            "%r tag takes at least one argument: the name of the template to "
            "be included." % bits[0]
        )
    return IncludeRawNode(parser.compile_filter(bits[1]))


@register.simple_tag(name='rendered_include')
def rendered_include(template, context):
    return template.render(context)
