from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from task.models import Task, TimeEntry
from task.forms import TimerForm
from team.models import Project
from datetime import datetime, timedelta
from json import dumps
from collections import OrderedDict


def get_date_str(date):
    return '%s-%s-%s' % (date.day, date.month, date.year)


def gen_reduce_group_by_day(timezone_offset):
    def reduce_group_by_day(first, second):
        date = get_date_str(second.time_start - timezone_offset)
        duration = second.time_end - second.time_start
        time_start = second.time_start - timezone_offset
        time_end = second.time_end - timezone_offset
        if first.get(date, False):
            first.get(date).get('tasks').append({
                'task': str(second.task),
                'project': str(second.task.project),
                'duration': duration,
                'time': '%02d:%02d - %02d:%02d' % (
                    time_start.hour, time_start.minute,
                    time_end.hour, time_end.minute)
                })
            first.get(date)['durationsum'] += duration
        else:
            first[date] = {
                'date': date,
                'durationsum': duration,
                'tasks': [{
                    'task': str(second.task),
                    'project': str(second.task.project),
                    'duration': duration,
                    'time': '%02d:%02d - %02d:%02d' % (
                        time_start.hour, time_start.minute,
                        time_end.hour, time_end.minute)
                    }]
                }
        return first
    return reduce_group_by_day


@login_required
def timer(request):
    week_ago = datetime.now() - timedelta(weeks=1)
    timezone_offset = timedelta(
            minutes=int(request.COOKIES.get('timezone', 0)))
    tasks = TimeEntry.objects.filter(
        user=request.user, time_start__gt=week_ago).order_by('-time_start')
    reduce_group_by_day = gen_reduce_group_by_day(timezone_offset)
    days = reduce(reduce_group_by_day, tasks, OrderedDict())

    day_template = loader.get_template('task/day.mustache')
    task_template = loader.get_template('task/task.mustache')
    form = TimerForm(user=request.user)
    return render(request, 'task/timer.html', {
        'task_template': task_template,
        'day_template': day_template,
        'days': days,
        'form': form,
        'page': 'timer'})


@login_required
def add_time_entry(request):
    if request.method == 'POST' and request.is_ajax():
        project = Project.objects.get(pk=request.POST.get('project'))
        try:
            task = project.tasks.get(name=request.POST.get('name'))
        except ObjectDoesNotExist:
            task = Task(
                name=request.POST.get('name'),
                status=1,
                project=project)
            task.save()
        time_start = datetime.fromtimestamp(
            int(request.POST.get('start_date')) / 1000)
        time_end = datetime.fromtimestamp(
            int(request.POST.get('end_date')) / 1000)
        time_entry = TimeEntry(
            task=task,
            time_start=time_start,
            time_end=time_end,
            user=request.user)
        time_entry.save()

        timezone_offset = timedelta(
            minutes=int(request.COOKIES.get('timezone', 0)))
        duration = time_end - time_start
        duration = '%d:%02d:%02d' % (
            duration.days * 24 + duration.seconds / 3600,
            (duration.seconds % 3600) / 60,
            duration.seconds % 60)
        time_start = time_start - timezone_offset
        time_end = time_end - timezone_offset
        response = {
            'date': get_date_str(time_start),
            'durationsum': duration,
            'tasks': [{
                'task': str(task),
                'project': str(project),
                'duration': duration,
                'time': '%02d:%02d - %02d:%02d' % (
                    time_start.hour, time_start.minute,
                    time_end.hour, time_end.minute)
                }]
            }
        return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('timer'))
