from django.conf.urls import url

urlpatterns = [
    url(r'^timer/$', 'task.views.timer', name='timer'),
    url(r'^timer/add/$', 'task.views.add_time_entry', name='add_time_entry'),
]
