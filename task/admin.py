from django.contrib import admin
from task.models import Task, TimeEntry

admin.site.register(Task)
admin.site.register(TimeEntry)
