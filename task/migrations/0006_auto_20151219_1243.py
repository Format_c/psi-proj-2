# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0005_auto_20151212_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.SmallIntegerField(default=0, choices=[(0, b'TODO'), (1, b'Started'), (2, b'Done'), (3, b'Cancelled')]),
        ),
    ]
