# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0002_auto_20151204_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='status',
            field=models.SmallIntegerField(default=0, choices=[(0, b'TODO'), (1, b'Started'), (2, b'Closed')]),
        ),
    ]
