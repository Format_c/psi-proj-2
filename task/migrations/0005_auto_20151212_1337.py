# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0004_auto_20151204_1655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.SmallIntegerField(default=0, choices=[(0, b'TODO'), (1, b'Started'), (2, b'Done'), (3, b'Canceled')]),
        ),
        migrations.AlterField(
            model_name='timeentry',
            name='task',
            field=models.ForeignKey(related_name='time_entries', to='task.Task'),
        ),
    ]
