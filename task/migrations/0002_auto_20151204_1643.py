# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_start', models.DateTimeField()),
                ('time_end', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Time entry',
                'verbose_name_plural': 'Time entries',
            },
        ),
        migrations.RemoveField(
            model_name='task',
            name='time_end',
        ),
        migrations.RemoveField(
            model_name='task',
            name='time_start',
        ),
        migrations.RemoveField(
            model_name='task',
            name='user',
        ),
        migrations.AddField(
            model_name='timeentry',
            name='task',
            field=models.ForeignKey(to='task.Task'),
        ),
        migrations.AddField(
            model_name='timeentry',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
