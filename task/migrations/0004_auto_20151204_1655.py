# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0003_task_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='project',
            field=models.ForeignKey(related_name='tasks', to='team.Project'),
        ),
        migrations.AlterField(
            model_name='timeentry',
            name='user',
            field=models.ForeignKey(related_name='time_entries', to=settings.AUTH_USER_MODEL),
        ),
    ]
