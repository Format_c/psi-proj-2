from django.forms import Form, CharField, ChoiceField, HiddenInput
from team.models import Team
from itertools import chain


def get_choices_project(user):
    projects = chain.from_iterable(
        [team.projects.all() for team in Team.objects.filter(users=user)])
    return [(project.pk, project.name) for project in projects]


class TimerForm(Form):
    def __init__(self, user=None, *args, **kwargs):
        super(TimerForm, self).__init__(*args, **kwargs)
        self.fields['name'] = CharField(max_length=200)
        self.fields['project'] = ChoiceField(choices=get_choices_project(user))
        self.fields['start_date'] = ChoiceField(widget=HiddenInput)
        self.fields['end_date'] = CharField(widget=HiddenInput)
