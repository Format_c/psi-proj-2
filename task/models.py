from django.db import models
from team.models import Project
from django.contrib.auth.models import User
from datetime import timedelta
from operator import add


class Task(models.Model):
    name = models.CharField(max_length=200, unique=True)
    status = models.SmallIntegerField(choices=(
        (0, 'TODO'),
        (1, 'Started'),
        (2, 'Done'),
        (3, 'Cancelled')
        ), default=0)
    project = models.ForeignKey(Project, related_name='tasks')

    class Meta:
        verbose_name = "Task"
        verbose_name_plural = "Tasks"

    def duration(self, user=None):
        # aggregate(Sum(F('time_end')-F('time_start'))) # nie dziala w SQLite
        if user:
            return reduce(add, [x.time_end - x.time_start for
                          x in self.time_entries.filter(user=user)],
                          timedelta(0))
        return reduce(add, [x.time_end - x.time_start for
                      x in self.time_entries.all()],
                      timedelta(0))

    def __unicode__(self):
        return self.name


class TimeEntry(models.Model):
    task = models.ForeignKey(Task, related_name='time_entries')
    time_start = models.DateTimeField()
    time_end = models.DateTimeField()
    user = models.ForeignKey(User, related_name='time_entries')

    class Meta:
        verbose_name = "Time entry"
        verbose_name_plural = "Time entries"

    def __unicode__(self):
        return self.task.name
