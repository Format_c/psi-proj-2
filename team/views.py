from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.middleware.csrf import get_token
from django.core.urlresolvers import reverse
from django.conf import settings
from team.forms import ProjectForm, FileForm, TeamForm
from team.models import Team, Project, File
from task.models import Task
from json import dumps
from itertools import chain


@login_required
def projects(request):
    projects = chain.from_iterable(
        [team.projects.all() for team in Team.objects.filter(
            users=request.user)])
    projects = [{
        'csrf': get_token(request),
        'mediaurl': settings.MEDIA_URL,
        'pk': project.pk,
        'name': str(project),
        'team': str(project.team),
        'duration': project.duration(request.user),
        'teamduration': project.duration(),
        'files': project.files.all(),
        'tasks': {
            'todo': [{
                'pk': task.pk,
                'name': str(task),
                'show_tick': True,
                'show_cross': True,
                'duration': task.duration(request.user),
                'teamduration': task.duration()}
                for task in project.tasks.filter(status=0)],
            'opened': [{
                'pk': task.pk,
                'name': str(task),
                'show_tick': True,
                'show_cross': True,
                'duration': task.duration(request.user),
                'teamduration': task.duration()}
                for task in project.tasks.filter(status=1)],
            'done': [{
                'pk': task.pk,
                'name': str(task),
                'show_tick': False,
                'show_cross': True,
                'duration': task.duration(request.user),
                'teamduration': task.duration()}
                for task in project.tasks.filter(status=2)],
            'cancelled': [{
                'pk': task.pk,
                'name': str(task),
                'show_tick': True,
                'show_cross': False,
                'duration': task.duration(request.user),
                'teamduration': task.duration()}
                for task in project.tasks.filter(status=3)]
        }} for project in projects]
    project_template = loader.get_template('team/project.mustache')
    task_template = loader.get_template('team/task.mustache')
    todo_form_template = loader.get_template('team/todo_form.mustache')
    file_template = loader.get_template('team/file.mustache')
    file_form_template = loader.get_template('team/add_file_form.mustache')
    empty_template = loader.get_template('team/empty.mustache')
    form = ProjectForm(user=request.user)
    return render(request, 'team/projects.html', {
        'project_template': project_template,
        'task_template': task_template,
        'todo_form_template': todo_form_template,
        'file_template': file_template,
        'file_form_template': file_form_template,
        'empty_template': empty_template,
        'projects': projects,
        'form': form,
        'page': 'projects'})


@login_required
def add_project(request):
    if request.method == 'POST':
        team = Team.objects.get(pk=request.POST.get('team'))
        project = Project(
            name=request.POST.get('name'),
            team=team)
        project.save()
        if request.is_ajax():
            response = {
                'csrf': get_token(request),
                'pk': project.pk,
                'name': str(project),
                'team': str(project.team),
                'duration': '0:00',
                'teamduration': '0:00'
                }
            return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('projects'))


@login_required
def add_task(request):
    if request.method == 'POST':
        project = Project.objects.get(pk=request.POST.get('project'))
        task = project.tasks.create(
            name=request.POST.get('task'),
            status=0)
        if request.is_ajax():
            response = {
                'csrf': get_token(request),
                'pk': task.pk,
                'name': str(task),
                'show_tick': True,
                'show_cross': True,
                'duration': '0:00',
                'teamduration': '0:00'
            }
            return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('projects'))


@login_required
def add_file(request):
    if request.method == 'POST':
        form = FileForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            uploaded_file = form.save()
            if request.is_ajax():
                response = {
                    'name': str(uploaded_file),
                    'file': uploaded_file.file
                }
                return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('projects'))


@login_required
def delete_file(request):
    if request.method == 'POST':
        File.objects.get(pk=request.POST.get('pk')).delete()
        if request.is_ajax():
            return HttpResponse('deleted')
    return HttpResponseRedirect(reverse('projects'))


@login_required
def delete_task(request):
    if request.method == 'POST':
        task = Task.objects.get(pk=request.POST.get('pk'))
        if task.status == 0:
            task.delete()
        else:
            task.status = 3
            task.save()
        if request.is_ajax():
            response = {
                'status': task.status
            }
            return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('projects'))


@login_required
def close_task(request):
    if request.method == 'POST':
        task = Task.objects.get(pk=request.POST.get('pk'))
        if task.status == 3:
            task.status = 1
        else:
            task.status = 2
        task.save()
        if request.is_ajax():
            response = {
                'status': task.status
            }
            return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('projects'))


@login_required
def teams(request):
    teams = [{
        'csrf': get_token(request),
        'teampk': team.pk,
        'name': str(team),
        'duration': team.duration(),
        'users': [{
            'pk': user.pk,
            'username': str(user),
            'duration': team.duration(user)
            } for user in team.users.all()]
        } for team in Team.objects.filter(users=request.user)]
    team_template = loader.get_template('team/team.mustache')
    user_template = loader.get_template('team/user.mustache')
    user_form_template = loader.get_template('team/add_user_form.mustache')
    form = TeamForm()
    return render(request, 'team/teams.html', {
        'team_template': team_template,
        'user_template': user_template,
        'user_form_template': user_form_template,
        'form': form,
        'teams': teams,
        'page': 'teams'})


@login_required
def add_team(request):
    if request.method == 'POST':
        team = Team(name=request.POST.get('name'))
        team.save()
        team.users.add(request.user)
        if request.is_ajax():
            response = {
                'csrf': get_token(request),
                'teampk': team.pk,
                'name': str(team),
                'duration': '0:00:00',
                'users': [{
                    'pk': request.user.pk,
                    'username': str(request.user),
                    'duration': '0:00:00'
                    }]
                }
            return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('teams'))


@login_required
def add_user(request):
    if request.method == 'POST':
        try:
            user = User.objects.get(username=request.POST.get('user'))
        except ObjectDoesNotExist:
            if request.is_ajax():
                raise Http404("User does not exist")
        else:
            team = Team.objects.get(pk=request.POST.get('team'))
            team.users.add(user)
            if request.is_ajax():
                response = {
                    'csrf': get_token(request),
                    'teampk': team.pk,
                    'pk': user.pk,
                    'username': str(user),
                    'duration': '0:00:00',
                }
                return HttpResponse(dumps(response))
    return HttpResponseRedirect(reverse('teams'))


@login_required
def remove_user(request):
    if request.method == 'POST':
        user = User.objects.get(pk=request.POST.get('user'))
        team = Team.objects.get(pk=request.POST.get('team'))
        team.users.remove(user)
        if request.is_ajax():
            return HttpResponse()
    return HttpResponseRedirect(reverse('teams'))
