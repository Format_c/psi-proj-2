from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta
from operator import add


class Team(models.Model):
    name = models.CharField(max_length=200)
    users = models.ManyToManyField(User, related_name='teams')

    class Meta:
        verbose_name = "Team"
        verbose_name_plural = "Teams"
        ordering = ['-pk']

    def duration(self, user=None):
        if user:
            return reduce(add, [x.duration(user=user) for
                          x in self.projects.all()],
                          timedelta(0))
        return reduce(add, [x.duration() for x in self.projects.all()],
                      timedelta(0))

    def __unicode__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=200)
    team = models.ForeignKey(Team, related_name='projects')

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"
        ordering = ['-pk']

    def duration(self, user=None):
        if user:
            return reduce(add, [x.duration(user=user) for
                          x in self.tasks.all()],
                          timedelta(0))
        return reduce(add, [x.duration() for x in self.tasks.all()],
                      timedelta(0))

    def __unicode__(self):
        return self.name


class File(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to='projects_files')
    project = models.ForeignKey(Project, related_name='files')

    class Meta:
        verbose_name = "File"
        verbose_name_plural = "Files"

    def __unicode__(self):
        return self.name
