# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('team', '0004_file_file'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='team',
            name='user',
        ),
        migrations.AddField(
            model_name='team',
            name='users',
            field=models.ManyToManyField(related_name='teams', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='file',
            name='project',
            field=models.ForeignKey(related_name='files', to='team.Project'),
        ),
        migrations.AlterField(
            model_name='project',
            name='team',
            field=models.ForeignKey(related_name='projects', to='team.Team'),
        ),
    ]
