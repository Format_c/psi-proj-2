from django.contrib import admin
from team.models import Project, Team, File

admin.site.register(Project)
admin.site.register(Team)
admin.site.register(File)
