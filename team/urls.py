from django.conf.urls import url

urlpatterns = [
    url(r'^projects/$', 'team.views.projects', name='projects'),
    url(r'^projects/add_project/$', 'team.views.add_project',
        name='add_project'),
    url(r'^projects/add_task/$', 'team.views.add_task', name='add_task'),
    url(r'^projects/close_task/$', 'team.views.close_task', name='close_task'),
    url(r'^projects/delete_task/$', 'team.views.delete_task',
        name='delete_task'),
    url(r'^projects/add_file/$', 'team.views.add_file', name='add_file'),
    url(r'^projects/delete_file/$', 'team.views.delete_file',
        name='delete_file'),
    url(r'^teams/$', 'team.views.teams', name='teams'),
    url(r'^teams/add_team/$', 'team.views.add_team', name='add_team'),
    url(r'^teams/add_user/$', 'team.views.add_user', name='add_user'),
    url(r'^teams/remove_user/$', 'team.views.remove_user', name='remove_user'),
]
