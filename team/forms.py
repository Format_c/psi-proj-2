from django.forms import Form, CharField, ChoiceField
from django.forms import ModelForm
from team.models import Team, File


def get_choices_team(user):
    return [(team.pk, team.name) for team in Team.objects.filter(users=user)]


class ProjectForm(Form):
    def __init__(self, user=None, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['name'] = CharField(max_length=200)
        self.fields['team'] = ChoiceField(choices=get_choices_team(user))


class TeamForm(Form):
    def __init__(self, *args, **kwargs):
        super(TeamForm, self).__init__(*args, **kwargs)
        self.fields['name'] = CharField(max_length=200)


class FileForm(ModelForm):
    class Meta:
        model = File
        fields = ('name', 'file', 'project', )
